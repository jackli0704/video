package cn.dream.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
public class TestCon {
    @GetMapping("/hello")
    public String test2(){
        return "hello";
    }
}
