package cn.dream;

import cn.dream.controller.TestCon;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        TestCon tc = (TestCon) ctx.getBean("tc");
        tc.test();
    }
}
